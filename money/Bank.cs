﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit.Samples.Money 
{
    public class Bank
    {
        private int m_Amount;

        public Bank()
        {
            m_Amount = 0;
        }

        public Bank(int m1)
        {
            m_Amount = m1;
        }

        public void deposit(int m2)
        {
            m_Amount += m2;
        }
        public void withdraw(int m3)
        {
            m_Amount -= m3;

        }
        public int getAmount()
        {
             
            return m_Amount;
        }
    }
}
