﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace NUnit.Samples.Money 
{
    [TestFixture]
    public class BankTest
    {
        private Money CashInHand;
        private Bank BankTT;


        [SetUp]
        protected void setup()
        {
            CashInHand = new Money(300, "USD");
            BankTT = new Bank(300);
        }

        [Test]
        public void test1st()
        {
            Assert.AreEqual(CashInHand.Amount, BankTT.getAmount());
        }

        [Test]
        public void testDeposit()
        {
            BankTT.deposit(CashInHand.Amount);

            
            Assert.AreEqual(BankTT.getAmount(), 600);

        }

        [Test]
        public void testCashInHand()
        {
            Money temp1 = new Money(300, "USD");

            Money expected = new Money(0, "USD");

            Assert.AreEqual(expected, CashInHand.Subtract(temp1));

        }
    }
}
