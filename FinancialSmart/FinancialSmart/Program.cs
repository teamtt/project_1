﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace FinancialSmart
{
    class Program
    {
        static void Main(string[] args)
        {
            String command = null;

            //for later development


            List<FinancialAccount> FinancialAccounts = new List<FinancialAccount>();

            FinancialAccounts.Add(new FinancialAccount("Chase CC", 123.4f, 2344.4f, 444.3f, "01/22/2015" ));
            FinancialAccounts.Add(new FinancialAccount("Citi CC", 123.4f, 2544.4f, 144.3f, "01/23/2015"));
            FinancialAccounts.Add(new FinancialAccount("Paypal CC", 123.4f, 2944.4f, 44.3f, "01/24/2015"));
            FinancialAccounts.Add(new FinancialAccount("Capital CC", 123.4f, 3044.4f, 244.3f, "01/26/2015"));
 

            Console.WriteLine("================================================================================");
            Console.WriteLine();

            Console.WriteLine("{0,25}{1}","","Pay Off My Debt");
            
            Console.WriteLine();

            Console.WriteLine("{0,10}{1,18}{2,18}{3,18}{4,5}", "FinancialAccount", "Starting Balance", "Current Balance", "Next Payment", "Date");

            Console.WriteLine();

            foreach (FinancialAccount aAcct in FinancialAccounts)
            {
                Console.WriteLine(aAcct);
            }

            Console.WriteLine("================================================================================");
            

            do{



               command = Console.ReadLine();
               Console.Clear();

            }            
            while(command.ToUpper() != "Q");

        }
    }

 

}
