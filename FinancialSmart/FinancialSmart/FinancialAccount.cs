﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialSmart
{
    public class FinancialAccount
    {   
        //Financial Account purpose is to keep track of financial obligation or payment
        private string _name;
        private float _starting_balance;
        private float _current_balance;
        private float _next_payment;
        private string _next_payment_date;


        public FinancialAccount(string name, float starting, float current, float next, string date)
        {
            Name = name;
            Starting_Balance = starting;
            Current_Balance = current;
            Next_Payment = next;
            Next_Payment_Date = date;
        }
        public override string ToString()
        {
            return String.Format("{0,-20}{1,-15}{2,-15}{3,-15}{4,-15}", Name, Starting_Balance, Current_Balance, Next_Payment, Next_Payment_Date);

        }

        //for testing assert.
        public bool MakePayment(float PayAmount)
        {
            Current_Balance = Current_Balance - PayAmount;
            return true;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public float Starting_Balance
        {
            get { return _starting_balance; }
            set { _starting_balance = value; }
        }

        public float Current_Balance
        {
            get { return _current_balance; }
            set { _current_balance = value; }
        }

        public float Next_Payment
        {
            get { return _next_payment; }
            set { _next_payment = value; }
        }

        public String Next_Payment_Date
        {
            get { return _next_payment_date; }
            set { _next_payment_date = value; }
        }

    }

}
