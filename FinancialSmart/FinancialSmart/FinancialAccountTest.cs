﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace FinancialSmart
{
    [TestFixture]
    public class FinancialAccountTest
    {
        private FinancialAccount fa;
        [SetUp]
        protected void setup()
        {
            fa = new FinancialAccount("Chase CC", 123.4f, 2344.4f, 444.3f, "01/22/2015");

        }

        [Test]
        public void TestMakePayment()
        {
            //test make payment to financial account 
            bool paymentReceived = fa.MakePayment(10);
            //and check the balance after the payment 
            float myBalance = fa.Current_Balance;
            //first test
            Assert.AreEqual(fa.MakePayment(10), true);
            Assert.AreEqual(fa.Current_Balance, 2324.4f);
        }
    }
}
