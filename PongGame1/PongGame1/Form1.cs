﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PongGame1
{
    public partial class PingGame : Form
    {
        private double vel, bhvel, hDir, vDir;

        public PingGame()
        {
            InitializeComponent();
        }

        private void PingGame_Load(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.URL = "chill.mp3";
            vel = 2;
            vDir = 1;
            hDir = 1;
            bhvel = vel;
        }

        private void field_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X >= field.Right - Player.Width)
            {
                Player.Left = field.Right - Player.Width;
            }
            else
            {
                Player.Left = e.X;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Ball.Left = Ball.Left + (int)(hDir * bhvel);
            Ball.Top = Ball.Top + (int)(vDir * vel);

            if (Ball.Left + Ball.Width >= field.Left + field.Width)
            {
                if (hDir > 0)
                    hDir = -hDir;
            }
            if (Ball.Top >= field.Top + field.Height)
            {
                if (vDir >0)
                    vDir = -vDir;
            }
            if (Ball.Left <= field.Left)
            {
                if (hDir < 0)
                    hDir = -hDir;
            }
            if (Ball.Bottom <= field.Height - field.Height)
            {
                if (vDir < 0)
                    vDir = -vDir;
            }
            if (Ball.Bottom >= Player.Top && Ball.Left >= (Player.Left - Ball.Width))
            {
                if (Ball.Right <= (Player.Right + Ball.Width))
                {
                    if (vDir > 0)
                        vDir = -vDir;
                    if (Ball.Left >= (Player.Left - Ball.Width) && Ball.Left <= Player.Left)
                    {
                        //if the ball hits left corner
                        bhvel = 3;
                        if (hDir > 0)
                            hDir = -hDir;
                    }
                    else if (Ball.Right <= (Player.Right + Ball.Width) && Ball.Right >= Player.Right)
                    {
                        //if the ball hits right corner
                        bhvel = 3;
                        if (hDir < 0)
                            hDir = -hDir;
                    }
                    else
                    {
                        bhvel = 2;
                    }

                }
            }
            if (Opponent.Left >= field.Right - Opponent.Width && Opponent.Left + (Opponent.Width / 2) < Ball.Left + (Ball.Width / 2))
            {
                Opponent.Left = field.Right - Opponent.Width;
            }
            else if (Opponent.Left <= field.Left && Opponent.Left + Opponent.Width / 2 > Ball.Left + Ball.Width / 2)
            {
                Opponent.Left = field.Left;
            }
            else
            {
                
                if (Opponent.Left + (Opponent.Width / 2) < Ball.Left + (Ball.Width / 2))
                {
                    Opponent.Left = Opponent.Left + (int)vel;
                }
                else if (Opponent.Left + Opponent.Width / 2 > Ball.Left + Ball.Width / 2)
                {
                    Opponent.Left = Opponent.Left - (int)vel;
                }
                else
                {
                    Opponent.Left = Ball.Left - (Opponent.Width - Ball.Width) / 2;
                }
                
            }

            if (Ball.Top <= Opponent.Bottom && Ball.Left >= (Opponent.Left - Ball.Width))
            {
                if (Ball.Right <= (Opponent.Right + Ball.Width))
                {
                    if (vDir < 0)
                        vDir = -vDir;
                    if (Ball.Left >= (Opponent.Left - Ball.Width) && Ball.Left <= Opponent.Left)
                    {
                        //if the ball hits left corner
                        bhvel = 3;
                        if (hDir > 0)
                            hDir = -hDir;
                    }
                    else if (Ball.Right <= (Opponent.Right + Ball.Width) && Ball.Right >= Opponent.Right)
                    {
                        //if the ball hits right corner
                        bhvel = 3;
                        if (hDir < 0)
                            hDir = -hDir;
                    }
                    else
                    {
                        bhvel = 2;
                    }

                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pong Game v 1.0\n by Trai, Tiffany, Ray", " Pong Game");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Run(new PingGame());
        }


    }
}
